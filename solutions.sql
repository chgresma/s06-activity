--Launching the MySQL Command-Line Client
mysql -u root;

--CREATING THE DATABASE
CREATE DATABASE blog_db;

USE blog;

--THE "AUTHOR" TABLE 
CREATE TABLE author(
    au_id INT NOT NULL AUTO_INCREMENT,
    au_lname VARCHAR(30) NOT NULL,
    au_fname VARCHAR(30) NOT NULL,
    address VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    state VARCHAR(10) NOT NULL,
    PRIMARY KEY(au_id)
);

--THE "PUBLISHER" TABLE 
CREATE TABLE publisher(
    pub_id INT NOT NULL AUTO_INCREMENT,
    pub_name VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    PRIMARY KEY(pub_id)
);

--THE "TITLE" TABLE with a FOREIGN KEY pub_id
CREATE TABLE title(
    title_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    type VARCHAR(30) NOT NULL,
    price FLOAT,
    pub_id INT NOT NULL,
    PRIMARY KEY(title_id),
    CONSTRAINT fk_title_pub_id
        FOREIGN KEY(pub_id) REFERENCES publisher(pub_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

--JOINING of the "author_title" TABLE 
CREATE TABLE author_title(
    id INT NOT NULL AUTO_INCREMENT,
    au_id VARCHAR(50) NOT NULL,
    title_id VARCHAR(50) NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_author_title_au_id
        FOREIGN KEY(au_id) REFERENCES author(au_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_author_id_title_id
        FOREIGN KEY(title_id) REFERENCES title(title_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

--CREATE TABLE "users"
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT ,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(25) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id)
);

--CREATE TABLE "posts"
CREATE TABLE posts(
   id INT NOT NULL AUTO_INCREMENT,
   user_id INT NOT NULL,
   title VARCHAR(100) NOT NULL,
   content VARCHAR(100) NOT NULL,
   datetime_posted DATETIME NOT NULL,
   PRIMARY KEY(id),
   CONSTRAINT fk_users_id
   FOREIGN KEY(user_id) REFERENCES users(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT 
);

--CREATE TABLE "post_comments"
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

--CREATE TABLE "post_likes"
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_post_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);


-- THE LIST OF THE BOOKS AUTHORED BY Marjorie Green
SELECT title
FROM title
JOIN author_title ON title.title_id = author_title.title_id
JOIN author ON author.au_id = author_title.au_id
WHERE author.au_fname = 'Marjorie' AND author.au_lname = 'Green';

-- THE LIST OF THE BOOKS AUTHORED BY Michael O'Leary
SELECT title
FROM title
JOIN author_title ON title.title_id = author_title.title_id
JOIN author ON author.au_id = author_title.au_id
WHERE author.au_fname = 'Michael' AND author.au_lname = 'O"Leary';

-- WRITE THE AUTHOR/S OF "The Busy Executives Database Guide"
SELECT author.au_fname, author.au_lname
FROM author
JOIN author_title ON author.au_id = author_title.au_id
JOIN title ON title.title_id = author_title.title_id
WHERE title.title = 'The Busy Executives Database Guide';

-- IDENTIFY THE PUBLISHER OF "But Is It Uer Friendly?"
SELECT publisher.pub_name
FROM publisher
JOIN title ON publisher.pub_id = title.pub_id
WHERE title.title = 'But Is It User Friendly?';

-- THE LIST OF THE BOOKS PUBLISHED BY Algodata Infosystem
SELECT title.title
FROM title
JOIN publisher ON title.pub_id = publisher.pub_id
WHERE publisher.pub_name = 'Algodata Infosystems';